﻿using Grpc.Net.Client;
using GrpcService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MvcClient.Controllers;
using MvcClient.Models;

namespace MvcClient.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            //var input = new DeviceModel
            //{
            //    Name = "Ciagnik",
            //    SerialNumber = "1243wvcwrevwv",
            //    Type = "Pojazd"

            //};

            //var input2 = new DeviceModel
            //{
            //    Name = "Ciagnik2",
            //    SerialNumber = "1243wvcwrevwv",
            //    Type = "Pojazd2"

            //};
            //var reply1 = client.Insert(input);
            //var reply2 = client.Insert(input2);

            //var reply3 = client.GetAll(new Empty()).Devices;
            //var x = reply3[0];
            //var x2 = reply3[1];
            //int i = 0;


            var deviceController = new DeviceController();
            List<DeviceModel> model = new List<DeviceModel>();
            //var o1 = deviceController.Post(input);
            //var o2 = deviceController.Post(input2);
            try
            {
                model = deviceController.GetAll().ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(model);
        }

        public IActionResult Privacy()
        {
            //var chanel = GrpcChannel.ForAddress("https://localhost:5001");
            //var client = new Device.DeviceClient(chanel);

            //var input = new DeviceModel
            //{
            //    Name = "Ciagnik",
            //    SerialNumber = "1243wvcwrevwv",
            //    Type = "Pojazd"

            //};

            //var input2 = new DeviceModel
            //{
            //    Name = "Ciagnik2",
            //    SerialNumber = "1243wvcwrevwv",
            //    Type = "Pojazd2"

            //};
            ////var reply1 = client.Insert(input);
            ////var reply2 = client.Insert(input2);

            ////var reply3 = client.GetAll(new Empty()).Devices;
            ////var x = reply3[0];
            ////var x2 = reply3[1];
            ////int i = 0;


            //var deviceController = new DeviceController();
            //var o1=deviceController.Post(input);
            //var o2= deviceController.Post(input2);
            //var model = deviceController.GetAll().ToList();
            ////model.Add(input);
            //int i = 0;

            return View();
        }

        [HttpPost]
        public ActionResult Index(DeviceModel device)
        {
            var deviceController = new DeviceController();
            //var o1 = deviceController.Post(input);
            //var o2 = deviceController.Post(input2);
            List<DeviceModel> model = new List<DeviceModel>();
            try
            {
                var result = deviceController.Post(device);
                model = deviceController.GetAll().ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return View(model);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
