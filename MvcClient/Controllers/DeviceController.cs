﻿using Grpc.Net.Client;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrpcService;

namespace MvcClient.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DeviceController : Controller
    {
        private readonly GrpcChannel channel;

        public DeviceController()
        {
            channel = GrpcChannel.ForAddress("https://localhost:5001");
        }

        [HttpGet]
        public List<DeviceModel> GetAll()
        {
            var client = new Device.DeviceClient(channel);
            return client.GetAll(new Empty()).Devices.ToList();
        }

        [HttpPost]
        public IActionResult Post([FromBody] DeviceModel  device)
        {
            var client = new Device.DeviceClient(channel);
            var newDevice = client.Insert(device);

            return CreatedAtRoute("GetDevice", new { id = newDevice.DeviceId }, newDevice);
        }
    }
}
