﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrpcService.Services
{
    public class DeviceService : Device.DeviceBase
    {
        private readonly List<DeviceModel> _devices = new List<DeviceModel>();
        private readonly ILogger<DeviceService> _logger;
        private int idCount = 0;

        public DeviceService(ILogger<DeviceService> logger)
        {
            _logger = logger;
        }

        public override Task<DeviceList> GetAll(Empty empty, ServerCallContext context)
        {
            DeviceList dl = new DeviceList();
            dl.Devices.AddRange(_devices);
            return Task.FromResult(dl);
        }

        public override Task<DeviceModel> Insert(DeviceModel device, ServerCallContext context)
        {
            device.DeviceId = idCount++;
            _devices.Add(device);
            return Task.FromResult(device);
        }

    }
}
